# HEXAH: a minimal implementation

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6394538.svg)](https://doi.org/10.5281/zenodo.6394538)

This is a companion code for the paper by **Reyne and Barlat (2022)** entitled 
***A new concept for continuum distortional plasticity*** 
and published in *International Journal of Plasticity* (2022).

The **citable version** is available at: doi.org/10.5281/zenodo.6394538

# Installation

This is a python module defined in a unique script. No specific means of installation, instead make sure to execute code from the same directory as the module file `hexah_minimal.py`.

```bash
.
├── hexah_minimal.py  # ~~ MODULE ~~ #
├── example_fig_11.py # example file
├── helpers.py        # bonus module (yld2000 , etc. definition)
├── LICENSE
└── README.md

```

# Usage

Instantiate the model and print parameters.
```python
import hexah_minimal as hexah
# See parameters (summarized in paper-table 1)
print(hexah.parameters)
# See variable names
print(hexah.variables.keys())
```
Set parameters and functions.
```python
import numpy as np

# Set parameters
hexah.parameters.update(q=4, p=6, γ=4)

# Set functions
von_mises = lambda s: np.sqrt(1.5)*np.linalg.norm(np.dot(s,hexah.PD))
power_law = lambda p: 1000*p**.5

hexah.functions.update(
    effective_stress=von_mises,# or Hill48, yld2000, ... (see `helpers.py`)
    hardening_rule=power_law,  # or Swift, Voce, etc.
)
```
Now integration requires 
- a loading direction (Bechterew/Kelvin tensorial basis), here `np.array([1,0,0,0,0,0])`;
- an increment size for the accumulated plastic strain, here `1e-2`.
```python
# Load one percent strain in tension
hexah.update_variables(np.array([1,0,0,0,0,0]), 1e-2)
```
See `example_*.py` for more thorough use cases.

# Licence

[MIT](https://choosealicense.com/licenses/mit/), © 2022 [Baptiste REYNE](https://orcid.org/0000-0003-2310-864X).
