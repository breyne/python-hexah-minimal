##################################################################### MIT License #
#                                                                                 #
#  Copyright (c) 2022 Baptiste REYNE (https://orcid.org/0000-0003-2310-864X)      #
#                                                                                 #
#  Permission is hereby granted, free of charge, to any person obtaining a copy   #
#  of this software and associated documentation files (the "Software"), to deal  #
#  in the Software without restriction, including without limitation the rights   #
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      #
#  copies of the Software, and to permit persons to whom the Software is          #
#  furnished to do so, subject to the following conditions:                       #
#                                                                                 #
#  The above copyright notice and this permission notice shall be included in all #
#  copies or substantial portions of the Software.                                #
#                                                                                 #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    #
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  #
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  #
#  SOFTWARE.                                                                      #
#                                                                                 #
###################################################################################

r""" Companion code for the paper by Reyne and Barlat (2022).

This script is an example of use for the model to reproduce
figure 11 (section 4.2) from the paper.
"""

import copy
import numpy as np
import pandas as pd
import plotly.graph_objects as go
import hexah_minimal as hexah
#import helpers# for fancier functions

###################################################################################
## Initializations ----------------------------------------------------------------
###################################################################################

# Set parameters from table 1, for section 3.1 ------------------------------------

hexah.parameters.update({
    # Exponents
    "q": 2.,
    "p": 2.,
    # Microstructure deviators
    "χmax": np.pi/6.,
    "ρh": 100.0,
    # Bauschinger effect
    "τb": .3,
    "ρb": 90.,
    "β": 1.1,
    # Permanent softening
    "τp": .85,
    "ρp": 15.,
    # Cross loading contraction
    "τc": 0.6,
    "ρc": 80.,
    "γ": 4.0,
    # Latent hardening
    "τl": 1.05,
    "εl": 0.01,
    "εpre":0.05,
    })

# Define and set base functions ---------------------------------------------------

def von_mises(stress):
    """Mises effective stress"""
    return np.sqrt(1.5)*np.linalg.norm(np.dot(stress,hexah.PD))

def hocket_sherby(aps):
    """Reference stress from Accumulated Plastic Strain (aps).
    Parameters from table 2, DP780 (section 3.1)
    """
    sigs    = 1028
    sigy    = 420.
    eta     = .57
    eps_eta = .12
    coef    = 355.
    return sigs - (sigs-sigy) * np.exp(-aps**eta/eps_eta) + coef*aps

hexah.functions.update({
    "effective_stress":von_mises,
    #"effective_stress":helpers.yld2004(**{'a':6, 'c`44':1, 'c``32':.0}),
    "hardening_rule": hocket_sherby,
    })

###################################################################################
## Simulation of a multi-strain-path-change experiment ----------------------------
###################################################################################

# Parametrization -----------------------------------------------------------------

# Define tensorial directions
TENSION_1 = np.array([1,0,0,0,0,0])
SHEAR_23 = np.array([0,1,-1,0,0,0])

# Set computation parameters
APS_INCREMENT =.0001 #accum. plast. strain increment
VERBOSE = False

# Initialization of the storage of solutions --------------------------------------

SAVED_VARS = list(hexah.variables.keys())#('stress', 'plastic_strain', 'acc_pla_strain',)
SOLUTIONS = dict()
for key in SAVED_VARS:
    SOLUTIONS[key] = (hexah.variables[key],)

# Simulation  ---------------------------------------------------------------------

while hexah.variables["acc_pla_strain"]<.20:

    # The load direction depends on the step
    if hexah.variables["acc_pla_strain"]<.05:
        LOAD_DIRECTION = +TENSION_1
    elif hexah.variables["acc_pla_strain"]<.10:
        LOAD_DIRECTION = -SHEAR_23
    elif hexah.variables["acc_pla_strain"]<.15:
        LOAD_DIRECTION = +SHEAR_23
    else:
        LOAD_DIRECTION = -TENSION_1

    # Solve and update variables
    hexah.update_variables(LOAD_DIRECTION, APS_INCREMENT)

    # Save solutions
    for key in SAVED_VARS:
        SOLUTIONS[key] += (copy.deepcopy(hexah.variables[key]),)

# Post-processing -----------------------------------------------------------------

# Make a more convenient pandas.DataFrame
df = pd.DataFrame(SOLUTIONS)

# Compute additional scalar values
df["reference_stress"] = hexah.functions['hardening_rule'](df['acc_pla_strain'])
df["mises_stress"] = np.vectorize(von_mises)(df['stress'])


###################################################################################
## Plots --------------------------------------------------------------------------
###################################################################################

# Initialize Plotly figure object
fig = go.Figure()

# Plot the two curves
fig.add_trace(go.Scatter(
    name='reference stress',
    x=df["acc_pla_strain"],
    y=df["reference_stress"],
    mode='lines',
    fill='tozeroy',
    ))
fig.add_trace(go.Scatter(
    name='simulation',
    x=df["acc_pla_strain"],
    y=df["mises_stress"],
    mode='markers',
    ))

# Format and show
fig.update_layout(
    title="HEXAH: paper figure 11 (Reyne and Barlat, 2022)",
    xaxis_title="accumulated plastic strain (-)",
    yaxis_title="Mises stress (MPa)",
    font=dict(size=12,),
    )
fig.show()

###################################################################################
## Export -------------------------------------------------------------------------
###################################################################################

# Write some results to a file
# The list given in columns contains names from df.keys()

df.to_csv(
    'dat-example-wh.tsv',
    sep='\t',
    float_format='%.3e',
    index=False,
    columns=[
        'acc_pla_strain',
        'mises_stress',
        'reference_stress',
        ],
)
