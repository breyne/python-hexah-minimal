##################################################################### MIT License #
#                                                                                 #
#  Copyright (c) 2022 Baptiste REYNE (https://orcid.org/0000-0003-2310-864X)      #
#                                                                                 #
#  Permission is hereby granted, free of charge, to any person obtaining a copy   #
#  of this software and associated documentation files (the "Software"), to deal  #
#  in the Software without restriction, including without limitation the rights   #
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      #
#  copies of the Software, and to permit persons to whom the Software is          #
#  furnished to do so, subject to the following conditions:                       #
#                                                                                 #
#  The above copyright notice and this permission notice shall be included in all #
#  copies or substantial portions of the Software.                                #
#                                                                                 #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    #
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  #
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  #
#  SOFTWARE.                                                                      #
#                                                                                 #
###################################################################################
"""Bonus module with additional functions
(fancier equivalent stresses, hardening rules).
Not needed to run hexah.
"""

import numpy as np

###################################################################################
## Equivalent stress functions ----------------------------------------------------
###################################################################################

def yld2000_2d(**p):
    r"""Barlat yld2000-2D [1]: anisotropic,
    assumes plane stress σ_3i = σ_i3 = 0!

    Arguments
    ---------
    **p
        Keyword arguments for function parameters:
        a, α1, α2, α3, α4, α5, α6, α7, α8

    Returns
    -------
    callable
        A function that takes a 3D Bechterew stress and returns
        the equivalent yld2000_2d scalar.

    Notes
    -----

    f_yld =( |   X`_1 -   X`_2|^a  / 2
           + |2*X``_1 -   X``_2|^a / 2
           + |  X``_1 - 2*X``_2|^a / 2 )^(1/a)

    where X`  = L` :\sigma
          X`` = L``:\sigma

    with the 4th order operators L` and L`` in plane stress:

            | L11  L12   0 |             |sig11
        L ~ | L21  L22   0 | for sigma ~ |sig22
            |   0    0 L66 |             |sig12

    expressed in the parametric form

        / L`11 \   / 2/3   0    0 \
        | L`12 |   |-1/3   0    0 | / a1 \
        | L`21 | = |  0  -1/3   0 | | a2 |
        | L`22 |   |  0   2/3   0 | \ a7 /
        \ L`66 /   \  0    0    1 /

        / L``11 \   /-2  2  8 -2  0 \ / a3 \
        | L``12 |   | 1 -4 -4  4  0 | | a4 |
        | L``21 | = | 4 -4 -4  1  0 | | a5 | * (1/9)
        | L``22 |   |-2  8  2 -2  0 | | a6 |
        \ L``66 /   \ 0  0  0  0  9 / \ a8 /

    References
    ----------
    .. [1] Barlat et al., 2003: "Plane stress yield function for
        aluminum alloy sheets—part 1: theory"
        International Journal of Plasticity.
        doi.org/10.1016/S0749-6419(02)00019-0
    """
    # set default parameters
    p.setdefault('a', 2.)
    p.setdefault('α1', 1.)
    p.setdefault('α2', 1.)
    p.setdefault('α3', 1.)
    p.setdefault('α4', 1.)
    p.setdefault('α5', 1.)
    p.setdefault('α6', 1.)
    p.setdefault('α7', 1.)
    p.setdefault('α8', 1.)

    # Define function
    def function_(sig):
        # Initialize projectors
        tmp = np.array([
            [ 2/3,   0 , 0],
            [-1/3,   0 , 0],
            [  0 , -1/3, 0],
            [  0 ,  2/3, 0],
            [  0 ,   0 , 1],
            ]).dot(np.array([[
                p['α1'],
                p['α2'],
                p['α7'],
                ]]).T
            )
        transfo_a = np.array([
            [tmp[0,0], tmp[1,0],      0  ],
            [tmp[2,0], tmp[3,0],      0  ],
            [   0    ,      0  , tmp[4,0]],
            ])
        tmp = 1/9*np.array([
            [-2.,  2,  8, -2,  0,],
            [ 1., -4, -4,  4,  0,],
            [ 4., -4, -4,  1,  0,],
            [-2.,  8,  2, -2,  0,],
            [ 0.,  0,  0,  0,  9,],
            ]).dot(np.array([[
                p['α3'],
                p['α4'],
                p['α5'],
                p['α6'],
                p['α8'],
                ]]).T
            )
        transfo_b = np.array([
            [tmp[0,0], tmp[1,0],      0  ],
            [tmp[2,0], tmp[3,0],      0  ],
            [   0    ,      0  , tmp[4,0]],
            ])

        # make sure sig is in deviatoric
        sig_dev = np.dot(PD, sig)
        # make sure sig is in dimension 2
        sig_ = np.array( [sig_dev[0], sig_dev[1], sig_dev[5], ])

        x_a = np.dot(transfo_a, sig_)
        x_xx = x_a[0]
        x_yy = x_a[1]
        x_xy = x_a[2]
        va1 = (x_xx+x_yy + np.sqrt((x_xx-x_yy)**2+4*x_xy**2))/2
        va2 = (x_xx+x_yy - np.sqrt((x_xx-x_yy)**2+4*x_xy**2))/2

        x_b = np.dot(transfo_b,sig_)
        x_xx = x_b[0]
        x_yy = x_b[1]
        x_xy = x_b[2]
        vb1 = (x_xx+x_yy + np.sqrt((x_xx-x_yy)**2+4*x_xy**2))/2
        vb2 = (x_xx+x_yy - np.sqrt((x_xx-x_yy)**2+4*x_xy**2))/2
        #import pdb; pdb.set_trace()

        # Compute function
        return (np.abs(  va1 -  va2)**p['a']/2 +
                np.abs(2*vb1 +  vb2)**p['a']/2 +
                np.abs(  vb1 +2*vb2)**p['a']/2
               )**(1/p['a'])

    # Return neat instance
    return function_

def yld2004(**p):
    r"""Barlat yld2004 [1]: anisotropic, 18 parameters

    Arguments
    ---------
    **p
        Keyword arguments for function parameters:
        a, c`ij, c``ij (see below)

    Returns
    -------
    callable
        A function that takes a 3D Bechterew stress and returns
        the equivalent yld2004 scalar.

    Notes
    -----
    f_yld = (1/4*\sum_{i,j} |s`_i - s``_j|^a )^(1/a)

    where s`  = C` :\dev(\sigma)
          s`` = C``:\dev(\sigma)

    with the 4th order operators C` and C`` of the parametric form:

            |    0  -c12  -c13    0    0    0 |
            | -c21     0  -c23    0    0    0 |
        C ~ | -c31  -c32     0    0    0    0 |
            |    0     0     0  c44    0    0 |
            |    0     0     0    0  c55    0 |
            |    0     0     0    0    0  c66 |

    References
    ----------
    .. [1]  Barlat et. al., 2005: "Linear transfomation-based
        anisotropic yield functions".
        International Journal of Plasticity.
        doi.org/10.1016/j.ijplas.2004.06.004
    """
    # set default parameters
    p.setdefault('a', 2.)
    # Upper triangles
    p.setdefault('c`12', 1.)
    p.setdefault('c`13', 1.)
    p.setdefault('c`23', 1.)
    p.setdefault('c``12', 0.)
    p.setdefault('c``13', 0.)
    p.setdefault('c``23', 0.)
    # Lower triangles
    p.setdefault('c`21', 1.)
    p.setdefault('c`31', 1.)
    p.setdefault('c`32', 1.)
    p.setdefault('c``21', 0.)
    p.setdefault('c``31', 0.)
    p.setdefault('c``32', 0.)
    # Diagonals
    p.setdefault('c`44', 1.)
    p.setdefault('c`55', 1.)
    p.setdefault('c`66', 1.)
    p.setdefault('c``44', 0.)
    p.setdefault('c``55', 0.)
    p.setdefault('c``66', 0.)

    # Define function
    def function_(sig):
        transfo_a = np.array([
            [         0, -p['c`12'], -p['c`13'],         0,         0,         0],
            [-p['c`21'],          0, -p['c`23'],         0,         0,         0],
            [-p['c`31'], -p['c`32'],          0,         0,         0,         0],
            [         0,          0,          0, p['c`44'],         0,         0],
            [         0,          0,          0,         0, p['c`55'],         0],
            [         0,          0,          0,         0,         0, p['c`66']]
            ])
        transfo_b = np.array([
            [          0, -p['c``12'], -p['c``13'],          0,          0,         0],
            [-p['c``21'],           0, -p['c``23'],          0,          0,         0],
            [-p['c``31'], -p['c``32'],           0,          0,          0,         0],
            [          0,           0,           0, p['c``44'],          0,         0],
            [          0,           0,           0,          0, p['c``55'],         0],
            [          0,           0,           0,          0,          0, p['c``66']]
            ])

        #import pdb; pdb.set_trace()

        sig_dev = np.dot(PD, sig)
        cart_a = bechterew_to_matricial(np.dot(transfo_a, sig_dev))
        cart_b = bechterew_to_matricial(np.dot(transfo_b, sig_dev))

        vals_a, _ = np.linalg.eig(cart_a)
        vals_b, _ = np.linalg.eig(cart_b)

        my_sum = 0.
        for sa_ in vals_a:
            for sb_ in  vals_b:
                my_sum += np.abs(sa_-sb_)**p['a']
        return (my_sum/2.)**(1./p['a'])

    # Return neat instance
    return function_

###################################################################################
## Utils --------------------------------------------------------------------------
###################################################################################

def bechterew_to_matricial(vect):
    r"""Return the cartesian matrix of a tensor given in Bechterew basis
                mat        <---     vect

                                /        \
         /               \      |   t11  |  This is referred to as the Bechterew
         | t11  t12  t13 |      |   t22  |  basis also called Kelvin 'notation' [1].
         |      t22  t23 | <--- |   t33  |
         | sym.      t33 |      | √2 t23 |
         \               /      | √2 t13 |
                                | √2 t12 |
                                \        /
    References
    ----------
    .. [1] Francois, Chen, Coret: Elasticity and symmetry of triangular lattice materials.
           International Journal of Solids and Structures, 2017.1
    """
    mat = np.diag(vect[:3])
    mat[1,2] = vect[3]*SQRT2 #23
    mat[2,1] = vect[3]*SQRT2
    mat[0,2] = vect[4]*SQRT2 #13
    mat[2,0] = vect[4]*SQRT2
    mat[0,1] = vect[5]*SQRT2 #12
    mat[1,0] = vect[5]*SQRT2

    return mat

###################################################################################
## Tensors and algebra ------------------------------------------------------------
###################################################################################

SQRT2 = np.sqrt(2)

# Fourth order identity, equation (1)
I4 = np.eye(6)

# Hydrostatic projector
PH = np.array([
    [1, 1, 1, 0, 0, 0],
    [1, 1, 1, 0, 0, 0],
    [1, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    ]) / 3.

# Deviatoric projector
PD = I4 - PH
