##################################################################### MIT License #
#                                                                                 #
#  Copyright (c) 2022 Baptiste REYNE (https://orcid.org/0000-0003-2310-864X)      #
#                                                                                 #
#  Permission is hereby granted, free of charge, to any person obtaining a copy   #
#  of this software and associated documentation files (the "Software"), to deal  #
#  in the Software without restriction, including without limitation the rights   #
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      #
#  copies of the Software, and to permit persons to whom the Software is          #
#  furnished to do so, subject to the following conditions:                       #
#                                                                                 #
#  The above copyright notice and this permission notice shall be included in all #
#  copies or substantial portions of the Software.                                #
#                                                                                 #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    #
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  #
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  #
#  SOFTWARE.                                                                      #
#                                                                                 #
###################################################################################

r""" Companion code for the paper by Reyne and Barlat [1]

Contents
--------
This is the implementation of the model, section 2.

Notes
-----
The implementation uses a simple format for second and fourth order tensors.

        [t]        --->     {t}

                        /        \
 /               \      |   t11  |  This is referred to as the Bechterew
 | t11  t12  t13 |      |   t22  |  basis also called Kelvin 'notation' [2].
 |      t22  t23 | ---> |   t33  |
 | sym.      t33 |      | √2 t23 |
 \               /      | √2 t13 |  We use it because it conserves the inner
                        | √2 t12 |  product, [A]:[B] = {A}.{B}
                        \        /

References
----------
.. [1] THE PAPER
.. [2] Francois, Chen, Coret: Elasticity and symmetry of triangular lattice materials.
       International Journal of Solids and Structures, 2017.
"""
# pylint: disable=invalid-name
import numpy as np

###################################################################################
## Initialization of attributes ---------------------------------------------------
###################################################################################

# Default parameters, see table 1.
# (CAN be updated by the user)
parameters = {
    "q": 6.,  "p": 6.0,            # Exponents
    "ρh": 0., "χmax": np.pi/6.,    # Microstructure deviators
    "τb": 1., "ρb": 0., "β":1.,    # Bauschinger effect
    "τp": 1., "ρp": 0.,            # Permanent softening
    "τc": 1., "ρc": 0., "γ":1.,    # Cross loading contraction
    "τl": 1., "εl": 0., "εpre":1., # Latent hardening
}

# Initialize functions
# (MUST be given by the user)
functions = {
    "effective_stress":lambda tmp: 'Plz provide effective_stress=callable(stress_tensor)',
    "hardening_rule"  :lambda tmp: 'Plz provide hardening_rule=callable(acc_plast_strain)',
}

# Initialize internal variables
# (must NOT be modified by the user)
variables = {
    "stress": np.zeros(6),
    "plastic_strain": np.zeros(6),
    "acc_pla_strain": 0.,
    "H": list(),  # see equation (11)
    "g+": list(), # section 2.4.1
    "g-": list(), #    "    2.4.1
    "gc": list(), #    "    2.4.3
    "gp": 1.0,    #    "    2.4.2
    "gl": 1.0,    #    "    2.4.4
    "ε0":    (), # not actually a variable, we just store it here defined under (20)
    "cosχ0": (), # not actually a variable, we just store it here; needed for latent hardening
}


###################################################################################
## Functions definition ----------------------------------------------------------
###################################################################################

def hardening_rule(aps=None):
    """The strain-stress curve from experiments.

    Defines the reference stress denoted `σ_R` and introduced in section 2.1 of the paper.
    We use the Hocket-Sherby function in the applications.

    """
    # Use the current aps if not given
    aps = aps if aps else variables["acc_pla_strain"]
    return functions["hardening_rule"](aps)

def equivalent_stress(stress):
    """The HEXAH equivalent stress.

    Computes the expression (14) from the paper.
    """
    p = parameters
    v = variables

    # Initialize ξ and f for equation  (14)
    phi = functions["effective_stress"](stress)
    xi = phi ** p["p"]
    f = 0.0

    # Add every contribution as regularized sum
    for i, h in enumerate(v["H"]):

        # Update ξ, equations (15,16,17)
        Ppe = PROJ_PERP(h)
        Pci = 4 * (1 - v["gc"][i]) * Ppe
        phi_c = functions["effective_stress"](np.dot(stress, Pci))
        xi += phi_c ** p["p"]

        # Update f, equations (18,19)
        f_plus = SQRT32 * (1. / (v["g+"][i]**p["q"]) - 1.) ** (1./p["q"])
        f_mnus = SQRT32 * (1. / (v["g-"][i]**p["q"]) - 1.) ** (1./p["q"])
        s_h = np.dot(stress, h)
        f += (
            (f_plus * POS_PART(s_h ))** p["q"] +
            (f_mnus * POS_PART(-s_h))** p["q"]
        )** (1 / p["q"])
    xi = xi ** (1 / p["p"])

    # Return expression (14)
    return (xi ** p["q"] + f ** p["q"]) ** (1 / p["q"]) / v["gp"] / v["gl"]

def update_variables(stress_direction, dp):# pylint: disable=too-many-locals, too-many-statements
    """Update variables given a load increment

    Covers section 2.4 in the paper.

    Parameters
    ----------
    stress_direction: ndarray
        a second order tensorial direction; will be normalized anyways
    dp: float
        the increment of accumulated plastic strain

    Notes
    -----
    The dict `variables` is modified in-place, so it is up to the user
    to save the material history.
    """

    p = parameters  # shorthand
    v = variables   # shorthand
    n = len(v["H"]) # number of contributions

    # -----------------------------------------------------------------------------
    # Accumulated plastic strains -------------------------------------------------
    sigma_ref_old = hardening_rule()
    v['acc_pla_strain'] += dp
    sigma_ref_new = hardening_rule()
    sigma_ref_deriv = (sigma_ref_new-sigma_ref_old)/dp

    # -----------------------------------------------------------------------------
    # Stress using the function's homogeneity -------------------------------------

    # σ_eq(k*σ) = k*σ_eq(σ) = σ_ref(ε) ---> k=σ_ref(ε)/σ_eq(σ)
    v['stress'] = sigma_ref_new / equivalent_stress(stress_direction) * stress_direction

    s = np.dot(v['stress'],PD)  # deviatoric stress, equation (9)
    s_hat = s/np.linalg.norm(s) # normalized deviator
    cos_chi = -1. if n==0 else abs(np.dot(s_hat, v["H"][-1]))# equation (10)

    # -----------------------------------------------------------------------------
    # Add a new contribution when needed, equation (12) ---------------------------

    if cos_chi < np.cos(p["χmax"]):
        # Store the initial accumulated plastic strain value
        v["ε0"] = v["ε0"] + ( v["acc_pla_strain"], )
        # Store the initial phase
        v["cosχ0"] = v["cosχ0"] + ( cos_chi, )
        # Add variables corresponding to contributions
        v["H" ] = v["H" ] + [s_hat,]
        v["g+"] = v["g+"] + [1.0,]
        v["g-"] = v["g-"] + [1.0,]
        v["gc"] = v["gc"] + [1.0,]

    s_h = np.dot(s, v["H"][-1]) # projection of s onto the active h
    lam = np.sign(s_h)          # sign

    # -----------------------------------------------------------------------------
    # Plastic strain using normal flow rule and homogeneity -----------------------
    sigma_bar_deriv = equivalent_stress_derivative(v['stress'])
    plastic_strain_increment = dp * sigma_bar_deriv
    v['plastic_strain'] = v['plastic_strain'] + plastic_strain_increment

    # -----------------------------------------------------------------------------
    # Active contribution evolutions ----------------------------------------------

    # Active microstructure deviator, equation (13)
    Ppe = PROJ_PERP(v["H"][-1])
    v["H"][-1] = v["H"][-1] + dp * p['ρh'] * np.dot(Ppe, s_hat) * lam

    # Bauschinger effec, equation (21)
    if lam > 0:
        v["g+"][-1] = v["g+"][-1] + dp*p['ρb']*(1.     -v["g+"][-1]) / (v["g+"][-1]**p["β"])
        v["g-"][-1] = v["g-"][-1] + dp*p['ρb']*(p['τb']-v["g-"][-1]) / (v["g-"][-1]**p["β"])
    else:
        v["g+"][-1] = v["g+"][-1] + dp*p['ρb']*(p['τb']-v["g+"][-1]) / (v["g+"][-1]**p["β"])
        v["g-"][-1] = v["g-"][-1] + dp*p['ρb']*(1.     -v["g-"][-1]) / (v["g-"][-1]**p["β"])

    # Cross loading contraction, equation (25)
    v["gc"][-1] = v["gc"][-1] + dp * p['ρc']*(p['τc']-v["gc"][-1]) / (v["gc"][-1])**p["γ"]

    # Permanent softening
    # Compute ratio while handling the 0-division case; equation (22)
    reverse_ratio = np.linalg.norm(v['plastic_strain'])*np.linalg.norm(plastic_strain_increment)
    if reverse_ratio:
        reverse_ratio = np.dot(v['plastic_strain'], plastic_strain_increment)/reverse_ratio/2
    # Update the variable, equation (24)
    v["gp"] = v["gp"] + dp * max(
            p['ρp']*(p['τp']-v["gp"])*POS_PART(-reverse_ratio),
            -sigma_ref_deriv/sigma_ref_new * v["gp"]
    )

    # Latent hardening, equations (27-30)
    if n>1:
        rho_l = np.exp(p['εl'])/(np.exp(p['εl'])-1)
        weight = (v['ε0'][-1] - v['ε0'][-2])/p['εpre']
        weight *= POS_PART(np.sqrt(1-v["cosχ0"][-1]**2)-np.sin(p['χmax']))/(1-np.sin(p['χmax']))
        L = weight * (rho_l/(rho_l-1))**(rho_l-1) * (p["τl"]-1)
        gl_fun = lambda e, : 1+L*rho_l*np.exp(-rho_l*e)*(np.exp(e)-1)
        v["gl"] = gl_fun(v['acc_pla_strain']-v['ε0'][-1])
    else:
        v["gl"] = 1.

    # -----------------------------------------------------------------------------
    # Frozen contributions evolution: recover back to 1 ---------------------------
    for i in range(n-1):
        v["g+"][i] = v["g+"][i] + dp*p['ρb']*(1.-v["g+"][i]) / (v["g+"][i]**p["β"])
        v["g-"][i] = v["g-"][i] + dp*p['ρb']*(1.-v["g-"][i]) / (v["g-"][i]**p["β"])
        v["gc"][i] = v["gc"][i] + dp*p['ρc']*(1.-v["gc"][i]) / (v["gc"][i]**p["γ"])

    # -----------------------------------------------------------------------------
    # Cleanup useless contributions, section 4.5 ----------------------------------
    # Here  we could cleanup the contributions starting from the oldest if
    # equation (34) returns a low-enough value.

    # Adjust stress now that variables have been updated --------------------------
    v['stress'] = sigma_ref_new / equivalent_stress(stress_direction) * stress_direction

def equivalent_stress_derivative(stress):
    """The equivalent stress derivative.

    Numerical derivative by default.
    Can be redefined to provide an analytical form.
    """
    # differential size (expected unit is MPa)
    diff_len = 1E-3
    # Initialize and compute
    deriv = np.zeros(6)
    for ten in np.diag([1, 1, 1, SQRT2, SQRT2, SQRT2,]):
        eqs_plus = equivalent_stress(stress+.5*diff_len*ten)
        eqs_minus = equivalent_stress(stress-.5*diff_len*ten)
        deriv = deriv + ten * (eqs_plus-eqs_minus)/diff_len
    return deriv


###################################################################################
## Tensors and algebra ------------------------------------------------------------
###################################################################################

SQRT32 = np.sqrt(1.5)
SQRT2 = np.sqrt(2)

# Second order identity
I2 = np.array([1., 1., 1., 0., 0., 0.,])

# Fourth order identity, equation (1)
I4 = np.eye(6)

# Hydrostatic projector
PH = np.array([
    [1, 1, 1, 0, 0, 0],
    [1, 1, 1, 0, 0, 0],
    [1, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    ]) / 3.

# Deviatoric projector
PD = I4 - PH

# Parallel projector, equation (2)
PROJ_PARA = lambda A: np.outer(A, A)/np.linalg.norm(A)**2

# Perpendicular projector, equation (3)
PROJ_PERP = lambda A: I4 - PROJ_PARA(A)

# Positive part from equation (6)
POS_PART = lambda x: 0.0 if x < 0 else x
